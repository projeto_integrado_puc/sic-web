package com.sic.domain.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.client.RestTemplate;

import com.sic.domain.dto.CotacaoDTO;
import com.sic.domain.dto.CotacaoFinalizarDTO;
import com.sic.domain.dto.CotacaoItemDTO;
import com.sic.domain.dto.CotacaoVencedoraDTO;
import com.sic.domain.dto.EmailDTO;
import com.sic.domain.dto.FornecedorDTO;
import com.sic.domain.dto.ProdutoDTO;
import com.sic.domain.dto.TokenDTO;
import com.sic.domain.helper.ConstantesSIC;
import com.sic.domain.helper.CreateHeaders;
import com.sic.domain.model.Parametro;
import com.sic.web.util.DateUtil;
import com.sic.web.util.NumberUtil;

@Service
public class CotacaoService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private LegadoService legadoService;

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UsuarioSistemaService usuarioSistemaService;
	
	@Autowired
	private ParametroService parametroService;
	
	@Autowired
	private EmailService emailService;

	@Value("${url.gateway}")
	private String urlGateway;

	public String gravaCotacao(CotacaoDTO cotacao) throws URISyntaxException {
		List<ProdutoDTO> produtoDTO = legadoService.getProdutosSemEstoque();
		Map<String, ProdutoDTO> produtosGravar = new HashMap<>();

		for (ProdutoDTO p : cotacao.getProdutos()) {
			if (BooleanUtils.isTrue(p.isSelecionado())) {
				produtosGravar.put(p.getCodigo().trim(), p);
			}
		}

		if (!produtosGravar.isEmpty()) {
			List<Long> listProd = new ArrayList<>();
			Map<String, FornecedorDTO> mapFornecedores = new HashMap<String, FornecedorDTO>();

			for (ProdutoDTO p : produtoDTO) {
				if (produtosGravar.containsKey(p.getCodigo().trim())) {
					if (Objects.nonNull(p.getFornecedores()) && p.getFornecedores().length > 0) {
						produtosGravar.get(p.getCodigo().trim()).setFornecedores(p.getFornecedores());
						for (FornecedorDTO f : p.getFornecedores()) {
							if (!mapFornecedores.containsKey(f.getEmail().trim())) {
								mapFornecedores.put(f.getEmail().trim(), f);
							}
						}
					}
					listProd.add(p.getId());
				}
			}

			cotacao.setProdutos(new ArrayList<>(produtosGravar.values()));
			cotacao.setNomeComprador(this.usuarioSistemaService.getUserName());
			cotacao.setDtCotacao(new Date());

			// Cria usuário para fornecedores não cadastrados
			usuarioService.criarUsuario(mapFornecedores);
			// Gravar cotacao
			String codCotacao = gravar(cotacao);
			// Atualiza status dos produtos
			// legadoService.updateProdutosCotacao(listProd);
			// Envia e-mail da cotação para os fornecedores
			// emailService.enviaEmailCotacao(mapFornecedores, codCotacao,
			// cotacao.getFimDtCotacao());

			return codCotacao;
		}
		return "";
	}

	public Page<CotacaoDTO> findCotacaoPage(Pageable pageable) throws URISyntaxException {
		List<CotacaoDTO> cotacoes = loadCotacao(pageable);
		
		if(CollectionUtils.isEmpty(cotacoes)) {
			return new PageImpl<>(new ArrayList<>());
		}else {
			Long total = cotacoes.get(0).getTotalRegistros();
			return new PageImpl<CotacaoDTO>(cotacoes,pageable, total);
		}
	}

	private List<CotacaoDTO> loadCotacao(Pageable pageable) throws URISyntaxException {
		TokenDTO t = tokenService.getToken();

		String url = "";
		if (usuarioSistemaService.isUsuarioAdministrador()) {
			url = this.urlGateway.trim() + ConstantesSIC.LOAD_COTACAO + "/" + pageable.getPageNumber();
		} else {
			url = this.urlGateway.trim() + ConstantesSIC.LOAD_COTACAO + "/" + usuarioSistemaService.getCNPJ() + "/"
					+ pageable.getPageNumber();
		}

		RequestEntity<Void> request = RequestEntity.get(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).build();
		ResponseEntity<CotacaoDTO[]> response = null;
		try {
			response = restTemplate.exchange(request, CotacaoDTO[].class);
		}catch (Exception e) {
			
		}

		if (Objects.nonNull(response) && response.hasBody()) {
			List<CotacaoDTO> list = new ArrayList<>();
			for (CotacaoDTO cotacao : response.getBody()) {
				list.add(cotacao);
			}
			return list;
		} else {
			return new ArrayList<>();
		}
	}

	public void validaCotacao(CotacaoDTO cotacao, BindingResult result) {
		if (StringUtils.isBlank(cotacao.getFimDt())) {
			result.addError(new ObjectError("fim data", "Data fim cotação não inserida"));
		} else {
			try {
				Date dataFim = DateUtil.StringParseDate(cotacao.getFimDt());
				Calendar calNow = Calendar.getInstance();
				calNow.add(Calendar.DATE, 1);

				if (dataFim.before(calNow.getTime())) {
					result.addError(
							new ObjectError("fim data", "Data fim cotação tem que ser uma data superior a atual"));
				} else {
					cotacao.setFimDtCotacao(dataFim);
				}
			} catch (Exception e) {
				result.addError(new ObjectError("fim data", "Data fim cotação inválida."));
			}
		}

		if (CollectionUtils.isEmpty(cotacao.getProdutos())) {
			result.addError(new ObjectError("produtos", "Não existem produtos para serem cotados"));
		} else {
			boolean itemSelecionado = false;
			for (ProdutoDTO p : cotacao.getProdutos()) {
				if (p.isSelecionado()) {
					itemSelecionado = true;
					if(Objects.isNull(p.getQuantidade()) || p.getQuantidade() == 0) {
						result.addError(
								new ObjectError("quantidade", "Quantidade inválida para o produto " + p.getCodigo()));
					}
				}
			}

			if (!itemSelecionado) {
				result.addError(
						new ObjectError("produtos", "Para cotar é necessário pelo menos selecionar um produto"));
			}
		}
	}

	private String gravar(CotacaoDTO cotacao) throws URISyntaxException {
		if (Objects.nonNull(cotacao)) {
			TokenDTO t = tokenService.getToken();

			String url = this.urlGateway.trim() + ConstantesSIC.GRAVAR_COTACAO;

			RequestEntity<CotacaoDTO> request = RequestEntity.post(new URI(url))
					.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken()))
					.contentType(MediaType.APPLICATION_JSON).body(cotacao, CotacaoDTO.class);

			ResponseEntity<String> re = restTemplate.exchange(request, String.class);

			if (Objects.nonNull(re)) {
				return re.getBody();
			}
		}
		return "";
	}
	
	public void finalizaCotacao(Long idCotacao) throws URISyntaxException, ParseException {
		Parametro param = this.parametroService.loadParametroGeral();
		CotacaoFinalizarDTO cotFin = new CotacaoFinalizarDTO();
		cotFin.setId(idCotacao);
		cotFin.setPrioridadeUm(param.getPrioridadeUm());
		cotFin.setPrioridadeDois(param.getPrioridadeDois());
		cotFin.setPrioridadeTres(param.getPrioridadeTres());
			
		TokenDTO t = tokenService.getToken();

		String url = this.urlGateway.trim() + ConstantesSIC.COTACAO;
		
		RequestEntity<CotacaoFinalizarDTO> request = RequestEntity.put(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken()))
				.contentType(MediaType.APPLICATION_JSON).body(cotFin, CotacaoFinalizarDTO.class);

		ResponseEntity<CotacaoVencedoraDTO> re = restTemplate.exchange(request, CotacaoVencedoraDTO.class);

		if (Objects.nonNull(re) && re.hasBody()) {
			String bodyEmail = montaEmailCompras(re.getBody());
			EmailDTO email = new EmailDTO();
			email.setDestinatario(param.getEmailCompras());
			email.setAssunto("Cotação " + idCotacao.toString() + " finalizada");
			email.setTexto(bodyEmail);

			emailService.enviaEmail(email);
		}
	}
	
	public String montaEmailCompras(CotacaoVencedoraDTO cotacaoVencedora) throws ParseException{	
		if(!CollectionUtils.isEmpty(cotacaoVencedora.getItens())) {
			Map<String, List<CotacaoItemDTO>> mapItens = new HashMap<>();
			for(CotacaoItemDTO ci : cotacaoVencedora.getItens()) {
				if(mapItens.containsKey(ci.getCnpjFornecedor())) {
					mapItens.get(ci.getCnpjFornecedor()).add(ci);
				}else {
					List<CotacaoItemDTO> list = new ArrayList<>();
					list.add(ci);
					mapItens.put(ci.getCnpjFornecedor(), list);
					mapItens.get(ci.getCnpjFornecedor()).add(ci);
				}
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append("Caro(a) Responsável Setor de Compra \n\n")
			  .append("Cotação ").append(cotacaoVencedora.getId()).append(" \n")
			  .append("Data cotação : " + DateUtil.dataFormatada(cotacaoVencedora.getDtCotacao()) + " \n")
			  .append("Nome comprador: " + cotacaoVencedora.getNomeComprador() + " \n\n");
			
			for(String cnpj : mapItens.keySet()) {
				List<CotacaoItemDTO> list = mapItens.get(cnpj);
				sb.append("Fornecedor \n");
				sb.append("CNPJ:" + cnpj + "\n");
				sb.append("Nome: " + list.get(0).getNomeFornecedorVencedor() + "\n");
				sb.append("e-mail: " + list.get(0).getEmail() + "\n");
				sb.append("Produtos: \n");
				
				for(CotacaoItemDTO ciForn : list) {
					sb.append("Descrição: " + ciForn.getCodigoProduto() + " : " + ciForn.getDescricaoProduto() + "\n");
					sb.append("Quantidade:" + ciForn.getQuantidade() + "\n");
					sb.append("Valor Total: R$" + NumberUtil.BigDecimalParaString(ciForn.getValor()) + "\n");
					sb.append("Valor Frete: R$" + NumberUtil.BigDecimalParaString(ciForn.getFrete()) + "\n");
					sb.append("Data entrega: R$" + DateUtil.dataFormatada(ciForn.getDataEntrega()) + "\n");
				}
				sb.append("\n\n");	
			}
			
			sb.append("Pedido de compra gerado automaticametne a partir do sistema SIC Cotação.");
			return sb.toString();
		}
		
		
		return "";
	}
}
