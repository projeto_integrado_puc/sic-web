package com.sic.domain.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.domain.dto.FornecedorDTO;
import com.sic.domain.dto.ProdutoDTO;
import com.sic.domain.dto.TokenDTO;
import com.sic.domain.helper.ConstantesSIC;
import com.sic.domain.helper.CreateHeaders;

@Service
public class LegadoService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	@Value("${url.gateway}")
	private String urlGateway;

	public List<ProdutoDTO> getProdutosSemEstoque() throws URISyntaxException {
		List<ProdutoDTO> produtos = new ArrayList<>();
		TokenDTO t = tokenService.getToken();

		String url = this.urlGateway.trim() + ConstantesSIC.PRODUTO;

		RequestEntity<Void> request = RequestEntity.get(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).build();
		
		ResponseEntity<ProdutoDTO[]> re = restTemplate.exchange(request, ProdutoDTO[].class);

		if (Objects.nonNull(re) && re.hasBody()) {
			for(ProdutoDTO p : re.getBody()) {
				if(p.getDescricao().length() > 32) {
					p.setDescricao(p.getDescricao().substring(0, 30));
				}else {
					p.setDescricao(p.getDescricao());
				}
				produtos.add(p);
			}
		}
		
		return produtos;
	}
	
	public List<FornecedorDTO> getFornecedores() throws URISyntaxException {
		List<FornecedorDTO> fornecedores = new ArrayList<>();
		TokenDTO t = tokenService.getToken();

		String url = this.urlGateway.trim() + ConstantesSIC.CADASTRO_FORNECEDOR;

		RequestEntity<Void> request = RequestEntity.get(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).build();
		
		ResponseEntity<FornecedorDTO[]> re = restTemplate.exchange(request, FornecedorDTO[].class);

		if (Objects.nonNull(re) && re.hasBody()) {
			for(FornecedorDTO f : re.getBody()) {
				fornecedores.add(f);
			}
		}
		
		return fornecedores;
	}
	
	public void updateProdutosCotacao(List<Long> produtos) throws URISyntaxException {
		TokenDTO t = tokenService.getToken();

		String url = this.urlGateway.trim() + ConstantesSIC.PRODUTO;
		
		RequestEntity<List<Long>> request = RequestEntity.put(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken()))
				.contentType(MediaType.APPLICATION_JSON).body(produtos, List.class);

		restTemplate.exchange(request, void.class);
	}
}
