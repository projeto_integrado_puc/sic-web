package com.sic.domain.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.client.RestTemplate;

import com.sic.domain.dto.FornecedorPropostaDTO;
import com.sic.domain.dto.ItemPropostaDTO;
import com.sic.domain.dto.TokenDTO;
import com.sic.domain.helper.ConstantesSIC;
import com.sic.domain.helper.CreateHeaders;
import com.sic.web.util.DateUtil;
import com.sic.web.util.NumberUtil;

@Service
public class PropostaService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	@Value("${url.gateway}")
	private String urlGateway;

	public List<FornecedorPropostaDTO> loadProposta(String cnpj, Long idCotacao, boolean usuarioAdmin) throws URISyntaxException {
		List<FornecedorPropostaDTO> propostas = new ArrayList<>();
		TokenDTO t = tokenService.getToken();

		String url = this.urlGateway.trim() + ConstantesSIC.PROPOSTA_LOAD + "/" + cnpj + "/" + idCotacao + "/" + usuarioAdmin;

		RequestEntity<Void> request = RequestEntity.get(new URI(url))
				.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).build();
		
		ResponseEntity<FornecedorPropostaDTO[]> re = restTemplate.exchange(request, FornecedorPropostaDTO[].class);

		if (Objects.nonNull(re) && re.hasBody()) {
			for(FornecedorPropostaDTO fp : re.getBody()) {
				propostas.add(fp);
			}
		}
		
		return propostas;
	}
	
	public void validaProposta(List<ItemPropostaDTO> propostas, BindingResult result) throws ParseException {
		Calendar calNow = Calendar.getInstance();
		calNow.add(Calendar.DATE, 1);
		for(ItemPropostaDTO fornProposta : propostas){
			 
			if(StringUtils.isEmpty(fornProposta.getValor()) || NumberUtil.StringParaBigDecimal(fornProposta.getValor()).doubleValue() <= 0){
				result.addError(new ObjectError("valor", "Valor não inserido para o item " + fornProposta.getId() + "."));
			}
			if(StringUtils.isEmpty(fornProposta.getFrete()) || NumberUtil.StringParaBigDecimal(fornProposta.getFrete()).doubleValue() <= 0){
				result.addError(new ObjectError("frete", "Valor do frete não inserido para o item " + fornProposta.getId() + "."));
			}
			if(StringUtils.isEmpty(fornProposta.getDataEntrega())){
				result.addError(new ObjectError("data", "Data entrega não inserido para o item " + fornProposta.getId() + "."));
			}else {
				Date dataEntrega = DateUtil.StringParseDate(fornProposta.getDataEntrega());
				
				if (dataEntrega.before(calNow.getTime())) {
					result.addError(
							new ObjectError("fim entrega", "Data entrega do item " + fornProposta.getId() + " tem que ser uma data superior a atual"));
				}
			}
		}
	}
	
	public String gravar(FornecedorPropostaDTO[] propostas) throws URISyntaxException {
		if (Objects.nonNull(propostas)) {
			TokenDTO t = tokenService.getToken();

			String url = this.urlGateway.trim() + ConstantesSIC.PROPOSTA_GRAVA;
			

			RequestEntity<FornecedorPropostaDTO[]> request = RequestEntity.post(new URI(url))
					.headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken()))
					.contentType(MediaType.APPLICATION_JSON).body(propostas, FornecedorPropostaDTO[].class);

			ResponseEntity<String> re = restTemplate.exchange(request, String.class);

			if (Objects.nonNull(re)) {
				return re.getBody();
			}
		}
		return "";
	}
}
