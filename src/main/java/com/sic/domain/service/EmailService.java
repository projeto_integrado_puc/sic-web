package com.sic.domain.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.domain.dto.Destinatario;
import com.sic.domain.dto.EmailCotacaoDTO;
import com.sic.domain.dto.EmailDTO;
import com.sic.domain.dto.FornecedorDTO;
import com.sic.domain.dto.ParametroDTO;
import com.sic.domain.dto.TokenDTO;
import com.sic.domain.helper.ConstantesSIC;
import com.sic.domain.helper.CreateHeaders;

@Service
public class EmailService {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private TokenService tokenService;

	@Value("${url.gateway}")
	private String urlGateway;
	
	@Value("${url.sic}")
	private String urlSic;

	public void enviaEmail(EmailDTO email) {
		TokenDTO t = tokenService.getToken();
		try {
			envia(t, email);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		System.out.println("Email enviado.");
	}

	private void envia(TokenDTO t, EmailDTO email) throws URISyntaxException {
		if (Objects.nonNull(email)) {
			String url = this.urlGateway.trim() + ConstantesSIC.ENVIO_EMAIL_SIMPLES;
			
			RequestEntity<EmailDTO> request = RequestEntity.post(new URI(url)).
					headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).
					contentType(MediaType.APPLICATION_JSON).body(email, EmailDTO.class);
			
			ResponseEntity re = restTemplate.exchange(request, Void.class);						

			if(Objects.nonNull(re)) {
				System.out.println("E-mail enviado");
			}
		}
	}
	
	public void enviaListaEmail(List<EmailDTO> listaEmail) {
		TokenDTO t = tokenService.getToken();
		try {
			enviaListEmail(t, listaEmail);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		System.out.println("Email enviado.");
	}
	
	private void enviaListEmail(TokenDTO t, List<EmailDTO> email) throws URISyntaxException {
		if (Objects.nonNull(email)) {
			String url = this.urlGateway.trim() + ConstantesSIC.ENVIO_EMAIL_LISTA;
		
			RequestEntity<List<EmailDTO>> request = RequestEntity.post(new URI(url)).
					headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).
					contentType(MediaType.APPLICATION_JSON).body(email, List.class);
			
			ResponseEntity re = restTemplate.exchange(request, Void.class);						

			if(Objects.nonNull(re)) {
				System.out.println("E-mail enviado");
			}
		}
	}
	
	public void enviaEmailCotacao(Map<String, FornecedorDTO> mapFornecedores, String codCotacao, Date dataFim) {
		EmailCotacaoDTO email = new EmailCotacaoDTO();
		email.setAssunto("Cotação " + codCotacao);
		email.setTexto("Prezado fornecedor a Varejo MG gerou a cotação " + codCotacao + ", acesse o SIC no endereço " + urlSic + "  e preencha antes da data " + dataFim + " para participar desta cotação.");
		email.setDestinatarios(new ArrayList<Destinatario>());
		for(String emailForn : mapFornecedores.keySet()) {
			Destinatario destinatario = new Destinatario();
			destinatario.setEmail(emailForn);
			destinatario.setParametros(new ArrayList<ParametroDTO>());
			email.getDestinatarios().add(destinatario);
		}
		
		TokenDTO t = tokenService.getToken();
		try {
			enviaCotacao(t, email);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		System.out.println("Email enviado.");
	}
	
	private void enviaCotacao(TokenDTO t, EmailCotacaoDTO email) throws URISyntaxException {
		if (Objects.nonNull(email)) {
			String url = this.urlGateway.trim() + ConstantesSIC.ENVIO_EMAIL_COTACAO;
			
			RequestEntity<EmailCotacaoDTO> request = RequestEntity.post(new URI(url)).
					headers(CreateHeaders.getHttpHeaders("Bearer " + t.getToken())).
					contentType(MediaType.APPLICATION_JSON).body(email, List.class);
			
			ResponseEntity re = restTemplate.exchange(request, Void.class);						

			if(Objects.nonNull(re)) {
				System.out.println("E-mail enviado");
			}
		}
	}
}