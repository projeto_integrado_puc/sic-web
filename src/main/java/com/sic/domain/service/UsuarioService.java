package com.sic.domain.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.sic.domain.dto.EmailDTO;
import com.sic.domain.dto.FornecedorDTO;
import com.sic.domain.dto.RedefinirSenhaDTO;
import com.sic.domain.exception.EmailJaCadastradoException;
import com.sic.domain.exception.SenhaObrigatoriaUsuarioException;
import com.sic.domain.exception.UsuarioNaoCadastradoException;
import com.sic.domain.helper.Utils;
import com.sic.domain.model.OcupacaoEnum;
import com.sic.domain.model.Usuario;
import com.sic.domain.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarios;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private EmailService emailService;

	@Value("${url.sic}")
	private String urlSic;

	public List<Usuario> findAll() {
		return usuarios.findAll();
	}

	public Page<Usuario> findAll(Pageable pageable) {
		return usuarios.findAll(pageable);
	}
	
	public Page<Usuario> findAllByUserName(String userName, Pageable pageable) {
		return new PageImpl<>(usuarios.findAllByUsername(userName, pageable));
	}

	public Optional<Usuario> findById(Long id) {
		return usuarios.findById(id);
	}

	public void redefinirSenha(RedefinirSenhaDTO redefinir) {
		Optional<Usuario> usuarioOptional = getUsername(redefinir.getUsername());
		if (usuarioOptional.isPresent()) {
			Usuario usuario = usuarioOptional.get();

			String novaSenha = atualizarSenhaUsuario(usuario);

			EmailDTO email = new EmailDTO();
			email.setDestinatario(usuario.getUsername());
			email.setAssunto("SIC sua nova senha de acesso");
			email.setTexto("Caro usuário " + usuario.getNome() + " o SIC gerou nova senha de acesso:" + novaSenha);

			emailService.enviaEmail(email);

		} else {
			throw new UsuarioNaoCadastradoException();
		}

	}

	@Transactional
	public void criarUsuario(Map<String, FornecedorDTO> fornecedores) {
		List<EmailDTO> emailAEnviar = new ArrayList<EmailDTO>();
		List<Usuario> usuariosNovos = new ArrayList<Usuario>();
		
		for (FornecedorDTO f : fornecedores.values()) {
			Optional<Usuario> opUsuario = usuarios.findByUsername(f.getEmail().trim());
			if (opUsuario.isEmpty()) {
				Usuario usuario = new Usuario();
				usuario.setNome(f.getNome());
				usuario.setOcupacao(OcupacaoEnum.FORNECEDOR);
				String senha = Utils.gerarSenha();
				usuario.setSenha(passwordEncoder.encode(senha));
				usuario.setConfirmacaoSenha(usuario.getSenha());
				usuario.setUsername(f.getEmail().trim());
				usuario.setCnpj(f.getCnpj());
				
				usuariosNovos.add(usuario);
				emailAEnviar.add(montaEMailCadastrodeUsuario(usuario, senha));
			}
		}
		
		if(!usuariosNovos.isEmpty()) {
			for(Usuario u : usuariosNovos) {
				usuarios.save(u);
			}
			emailService.enviaListaEmail(emailAEnviar);
		}
	}

	public EmailDTO montaEMailCadastrodeUsuario(Usuario usuario, String senha) {
		EmailDTO email = new EmailDTO();			
		email.setDestinatario(usuario.getUsername());
		email.setAssunto("Usuário cadastro no sistema SIC");
		email.setTexto("Caro " + usuario.getNome() + " acesse o sistema SIC pelo endereço " + this.urlSic + " , utilizando seu e-mail como usuário e a senha:  " + senha );
		return email;
	}

	@Transactional
	public void salvar(Usuario usuario) {

		String senha = usuario.getSenha();

		Optional<Usuario> usuarioExistente = getUsername(usuario.getUsername());
		if (usuarioExistente.isPresent()) {
			throw new EmailJaCadastradoException("E-mail já cadastrado.");
		}

		if (usuario.isNovo() && StringUtils.isEmpty(senha)) {
			throw new SenhaObrigatoriaUsuarioException("Senha é obrigatória para novos usuários");
		}

		if (usuario.isNovo() || !StringUtils.isEmpty(senha)) {
			usuario.setSenha(this.passwordEncoder.encode(senha));
		} else if (StringUtils.isEmpty(senha)) {
			usuario.setSenha(usuarioExistente.get().getSenha());
		}
		usuario.setConfirmacaoSenha(usuario.getSenha());

		create(usuario);
	}

	private String atualizarSenhaUsuario(Usuario usuario) {
		String novaSenha = Utils.gerarSenha();
		usuario.setSenha(passwordEncoder.encode(novaSenha));
		usuarios.save(usuario);
		return novaSenha;
	}

	private Optional<Usuario> getUsername(String username) {
		return usuarios.findByUsername(username);
	}

	private void create(Usuario usuario) {
		usuarios.save(usuario);
	}

}
