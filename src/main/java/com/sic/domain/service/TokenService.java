package com.sic.domain.service;

import java.util.Calendar;
import java.util.Objects;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.sic.domain.dto.TokenDTO;
import com.sic.domain.dto.TokenRequestDTO;
import com.sic.domain.helper.ConstantesSIC;

@Service
public class TokenService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${sic.gateway.user}")
	private String username;

	@Value("${sic.gateway.password}")
	private String password;

	@Resource(name = "loadToken")
	private TokenDTO token;

	@Value("${url.gateway}")
	private String urlGateway;

	public TokenDTO getToken() {
		if (istokenInValido()) {
			obterToken();
		}
		return this.token;
	}

	private boolean istokenInValido() {
		Calendar hourNow = Calendar.getInstance();
		hourNow.add(Calendar.MINUTE, 2);
		if (Objects.isNull(this.token) || StringUtils.isBlank(this.token.getToken())
				|| (Objects.isNull(this.token.getExpira()) || this.token.getExpira().before(hourNow.getTime()))) {
			return true;
		}
		return false;
	}

	private void obterToken() {
		TokenRequestDTO token = new TokenRequestDTO();
		token.setUsername(this.username);
		token.setPassword(this.password);

		String url = this.urlGateway + ConstantesSIC.MS_TOKEN;

		ResponseEntity<TokenDTO> re = restTemplate.exchange(url, HttpMethod.POST,
				new HttpEntity<TokenRequestDTO>(token), TokenDTO.class);

		if (Objects.nonNull(re) && Objects.nonNull(re.getBody())) {
			this.token = re.getBody();
		}
	}
}
