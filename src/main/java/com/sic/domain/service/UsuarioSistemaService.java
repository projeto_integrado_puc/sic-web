package com.sic.domain.service;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sic.domain.model.OcupacaoEnum;
import com.sic.domain.model.Usuario;
import com.sic.domain.repository.UsuarioRepository;
import com.sic.web.security.UsuarioSistema;

@Service
public class UsuarioSistemaService {

	@Autowired
	private UsuarioRepository usuarios;

	public boolean isUsuarioAdministrador() {
		return OcupacaoEnum.ADMINISTRADOR.equals(getUsuario().getOcupacao()) ? true : false;
	}

	public String getUserName() {
		return getUsuario().getUsername();
	}

	public String getCNPJ() {
		return getUsuario().getCnpj();
	}
	
	private Usuario getUsuario() {
		UsuarioSistema us = (UsuarioSistema) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (Objects.nonNull(us) && Objects.nonNull(us.getUsuario())) {
			return us.getUsuario();
		}else {
			Optional<Usuario> opUsuario = usuarios.findByUsername(us.getUsername());
			if(opUsuario.isPresent()) {
				((UsuarioSistema) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).setUsuario(opUsuario.get());
				return opUsuario.get();
			}else {
				throw new UsernameNotFoundException("Usuário não logado.");
			}
		}
	}
}
