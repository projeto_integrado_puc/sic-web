package com.sic.domain.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.sic.domain.dto.ParametroGeralDTO;
import com.sic.domain.model.Parametro;
import com.sic.domain.model.PrioridadeEnum;
import com.sic.domain.model.TipoParametroEnum;
import com.sic.domain.repository.ParametroRepository;

@Service
public class ParametroService {

	@Autowired
	private ParametroRepository repository;

	public Parametro loadParametroGeral() {
		List<Parametro> parametros = repository.findAllByTipoParam(TipoParametroEnum.GERAL);
		Parametro parametro = null;
		if(CollectionUtils.isEmpty(parametros)) {
			parametro = new Parametro();
			parametro.setTipoParam(TipoParametroEnum.GERAL);
		}else {
			parametro = parametros.get(0);
		}
		return parametro;
	}
	
	public Parametro validaParametro(ParametroGeralDTO param, BindingResult result) {
		Parametro parametro = param.getParametro();
		if (StringUtils.isEmpty(parametro.getEmailCompras())) {
			result.addError(new ObjectError("email", "E-mail setor de compras não inserido."));
		}else {
			if(!isValidEmailAddressRegex(parametro.getEmailCompras())) {
				result.addError(new ObjectError("email", "E-mail formato inválido."));
			}
		}
		
		Map<PrioridadeEnum, Integer> prioridades = new HashMap<>();
		if(Objects.nonNull(parametro.getPrioridadeUm()) && !PrioridadeEnum.INFORME_PRIORIDADE.equals(parametro.getPrioridadeUm())) {
			prioridades.put(parametro.getPrioridadeUm(), 1);
		}
		if(Objects.nonNull(parametro.getPrioridadeDois()) && !PrioridadeEnum.INFORME_PRIORIDADE.equals(parametro.getPrioridadeDois())) {
			if(!prioridades.isEmpty() && prioridades.containsKey(parametro.getPrioridadeDois())) {
				result.addError(new ObjectError("prioridade", "Prioridade " + parametro.getPrioridadeDois().getDescricao() + " já informada."));
			}else {
				prioridades.put(parametro.getPrioridadeDois(), 2);
			}
		}
		if(Objects.nonNull(parametro.getPrioridadeTres()) && !PrioridadeEnum.INFORME_PRIORIDADE.equals(parametro.getPrioridadeTres())) {
			if(!prioridades.isEmpty() && prioridades.containsKey(parametro.getPrioridadeTres())) {
				result.addError(new ObjectError("prioridade", "Prioridade " + parametro.getPrioridadeTres().getDescricao() + " já informada."));
			}else {
				prioridades.put(parametro.getPrioridadeTres(), 3);
			}
		}
		
		if(prioridades.isEmpty()) {
			result.addError(new ObjectError("prioridade", "É necessário cadastrar pelo menos uma Prioridade."));
		}
		
		return parametro;
	}

	@Transactional
	public void salvar(Parametro parametro) {
		List<Parametro> parametros = repository.findAllByTipoParam(TipoParametroEnum.GERAL);
		Parametro parametroBD = (CollectionUtils.isEmpty(parametros)) ? new Parametro() : parametros.get(0);
		parametroBD.setTipoParam(TipoParametroEnum.GERAL);
		parametroBD.setEmailCompras(parametro.getEmailCompras());
		parametroBD.setPrioridadeUm(parametro.getPrioridadeUm());
		parametroBD.setPrioridadeDois(parametro.getPrioridadeDois());
		parametroBD.setPrioridadeTres(parametro.getPrioridadeTres());
		repository.save(parametroBD);
	}
	
	public boolean isValidEmailAddressRegex(String email) {
	    boolean isEmailIdValid = false;
	    if (email != null && email.length() > 0) {
	        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
	        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
	        Matcher matcher = pattern.matcher(email);
	        if (matcher.matches()) {
	            isEmailIdValid = true;
	        }
	    }
	    return isEmailIdValid;
	}
}
