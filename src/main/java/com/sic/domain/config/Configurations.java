package com.sic.domain.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.ApplicationScope;

import com.sic.domain.dto.TokenDTO;

@Configuration
public class Configurations {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	@Bean()
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
	@Bean
	@ApplicationScope
	public TokenDTO loadToken() {
	    return new TokenDTO();
	}
}
