package com.sic.domain.dto;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "Redefinir Senha")
public class RedefinirSenhaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Dados inválidos.")
	private String username;

}
