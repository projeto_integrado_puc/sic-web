package com.sic.domain.dto;

import java.io.Serializable;

import com.sic.domain.model.PrioridadeEnum;

public class CotacaoFinalizarDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private PrioridadeEnum prioridadeUm;
	private PrioridadeEnum prioridadeDois;
	private PrioridadeEnum prioridadeTres;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PrioridadeEnum getPrioridadeUm() {
		return prioridadeUm;
	}
	public void setPrioridadeUm(PrioridadeEnum prioridadeUm) {
		this.prioridadeUm = prioridadeUm;
	}
	public PrioridadeEnum getPrioridadeDois() {
		return prioridadeDois;
	}
	public void setPrioridadeDois(PrioridadeEnum prioridadeDois) {
		this.prioridadeDois = prioridadeDois;
	}
	public PrioridadeEnum getPrioridadeTres() {
		return prioridadeTres;
	}
	public void setPrioridadeTres(PrioridadeEnum prioridadeTres) {
		this.prioridadeTres = prioridadeTres;
	}
}