package com.sic.domain.dto;

import java.io.Serializable;

public class EmailDTO  implements Serializable {

	private static final long serialVersionUID = -7975411324686503363L;
	
	private String destinatario;
	private String assunto;
	private String texto;
	private String msgError;

	public String getMsgError() {
		return msgError;
	}

	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}

	@Override
	public String toString() {
		return "EmailDTO [destinatario=" + destinatario + ", assunto=" + assunto + ", texto=" + texto + ", msgError="
				+ msgError + "]";
	}
}
