package com.sic.domain.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.sic.web.util.DateUtil;
import com.sic.web.util.NumberUtil;

public class RespostaPropostaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String tituloCotacao;
	private List<ItemPropostaDTO> propostas;

	public RespostaPropostaDTO(String tituloCotacao, List<FornecedorPropostaDTO> fornProposta) throws ParseException {
		super();
		this.tituloCotacao = tituloCotacao;
		this.propostas = new ArrayList<ItemPropostaDTO>();
		
		if(!CollectionUtils.isEmpty(fornProposta)) {
			for(FornecedorPropostaDTO fp : fornProposta) {
				ItemPropostaDTO it = new ItemPropostaDTO();
				it.setId(fp.getId());
				it.setDataEntrega(DateUtil.dateParseString(fp.getDataEntrega()));
				it.setFrete(NumberUtil.BigDecimalParaString(fp.getFrete()));
				it.setNomeProduto(fp.getNomeProduto());
				it.setQuantidade(fp.getQuantidade());
				it.setValor(NumberUtil.BigDecimalParaString(fp.getValor()));
				this.propostas.add(it);
			}
		}
	}

	public String getTituloCotacao() {
		return tituloCotacao;
	}

	public void setTituloCotacao(String tituloCotacao) {
		this.tituloCotacao = tituloCotacao;
	}

	public List<ItemPropostaDTO> getPropostas() {
		return propostas;
	}

	public void setPropostas(List<ItemPropostaDTO> propostas) {
		this.propostas = propostas;
	}
	
	public FornecedorPropostaDTO[] getFornecedorProposta() throws ParseException{
		FornecedorPropostaDTO[] retorno = null;
		
		if(!CollectionUtils.isEmpty(this.getPropostas())) {
			retorno = new FornecedorPropostaDTO[this.getPropostas().size()];
			int index = 0;
			for(ItemPropostaDTO ip : this.getPropostas()) {
				FornecedorPropostaDTO fp = new FornecedorPropostaDTO();
				fp.setDataEntrega(DateUtil.StringParseDate(ip.getDataEntrega()));
				fp.setFrete(NumberUtil.StringParaBigDecimal(ip.getFrete()));
				fp.setId(ip.getId());
				fp.setValor(NumberUtil.StringParaBigDecimal(ip.getValor()));
				retorno[index] = fp;
				index++;
			}
		}
		return retorno;
	}
}
