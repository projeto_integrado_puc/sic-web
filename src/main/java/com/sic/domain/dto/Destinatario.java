package com.sic.domain.dto;

import java.io.Serializable;
import java.util.List;

public class Destinatario implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String email;
	private List<ParametroDTO> parametros;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<ParametroDTO> getParametros() {
		return parametros;
	}
	public void setParametros(List<ParametroDTO> parametros) {
		this.parametros = parametros;
	}
}
