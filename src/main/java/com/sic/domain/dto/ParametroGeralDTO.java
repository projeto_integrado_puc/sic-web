package com.sic.domain.dto;

import java.io.Serializable;

import com.sic.domain.model.Parametro;

public class ParametroGeralDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Parametro parametro;

	public Parametro getParametro() {
		return parametro;
	}

	public void setParametro(Parametro parametro) {
		this.parametro = parametro;
	}
}
