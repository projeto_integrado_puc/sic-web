package com.sic.domain.dto;

import java.io.Serializable;
import java.util.Date;

public class TokenDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String username;
	private String token;
	private Date expira;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getExpira() {
		return expira;
	}

	public void setExpira(Date expira) {
		this.expira = expira;
	}
}