package com.sic.domain.dto;

import java.io.Serializable;
import java.util.Date;

public class TokenRequestDTO implements Serializable {

	private static final long serialVersionUID = -5205195632487814939L;

	private String username;
	private String password;
	private Date expira;
	private String token;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getExpira() {
		return expira;
	}

	public void setExpira(Date expira) {
		this.expira = expira;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}	
}