package com.sic.domain.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sic.domain.model.Parametro;
import com.sic.domain.model.TipoParametroEnum;

@Repository
public interface ParametroRepository extends JpaRepository<Parametro, Long> {
	
	public List<Parametro> findAllByTipoParam(TipoParametroEnum tipoParam);
}