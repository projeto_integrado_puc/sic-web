package com.sic.domain.exception;

public class UsuarioNaoCadastradoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UsuarioNaoCadastradoException(String message) {
		super(message);
	}

	public UsuarioNaoCadastradoException() {
		super("Não foi encontrado usuário cadastrado com esse e-mail.");
	}

}
