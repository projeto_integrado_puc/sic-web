package com.sic.domain.exception;

public class FalhaBancoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public FalhaBancoException(String message) {
		super(message);
	}

	public FalhaBancoException(String message, Throwable cause) {
		super(message, cause);

	}

}
