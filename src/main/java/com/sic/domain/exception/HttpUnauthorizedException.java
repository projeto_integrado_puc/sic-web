/**
 * 
 */
package com.sic.domain.exception;

/**
 * @author marcio.brandao
 *
 */
public class HttpUnauthorizedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public HttpUnauthorizedException(String arg0) {
		super(arg0);
	}

}
