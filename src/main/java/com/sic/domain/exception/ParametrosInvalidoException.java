package com.sic.domain.exception;

public class ParametrosInvalidoException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ParametrosInvalidoException() {
		super("Parâmetros inválidos.");
	}

}
