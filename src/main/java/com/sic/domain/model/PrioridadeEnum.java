package com.sic.domain.model;

import lombok.Getter;

@Getter
public enum PrioridadeEnum {

	INFORME_PRIORIDADE("Informe Prioridade"),DATA_ENTREGA("Data entrega"), REPUTACAO_FORNECEDOR("Reputação Fornecedor"), VALOR("Valor");

	private String descricao;

	PrioridadeEnum(String descricao) {
		this.descricao = descricao;
	}

}
