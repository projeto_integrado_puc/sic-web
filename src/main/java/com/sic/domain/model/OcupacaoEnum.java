package com.sic.domain.model;

import lombok.Getter;

@Getter
public enum OcupacaoEnum {

	ADMINISTRADOR("Administrador"), FORNECEDOR("Fornecedor");

	private String descricao;

	OcupacaoEnum(String descricao) {
		this.descricao = descricao;
	}

}
