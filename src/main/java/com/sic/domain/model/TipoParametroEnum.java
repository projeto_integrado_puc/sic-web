package com.sic.domain.model;

import lombok.Getter;

@Getter
public enum TipoParametroEnum {

	GERAL("Geral"), COTACAO("Cotação");

	private String descricao;

	TipoParametroEnum(String descricao) {
		this.descricao = descricao;
	}

}
