package com.sic.domain.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.sic.core.validation.AtributoConfirmacao;

import lombok.Data;

@Data
@Entity
@AtributoConfirmacao(atributo = "senha", atributoConfirmacao = "confirmacaoSenha", message = "Confirmação da senha não confere.")
@Table(name = "usuario")
@DynamicInsert
@DynamicUpdate
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "login precisa ser preenchido.")
	private String username;

	@NotEmpty(message = "nome precisa ser preenchido.")
	private String nome;

	@NotNull(message = "ocupação precisa ser preenchida.")
	@Enumerated(EnumType.STRING)
	private OcupacaoEnum ocupacao;

	@NotEmpty(message = "senha precisa ser preenchida.")
	private String senha;
	
	private String cnpj;

	@Transient
	private String confirmacaoSenha;

	@PreUpdate
	private void preUpdate() {
		this.confirmacaoSenha = senha;
	}

	public boolean isNovo() {
		return Objects.isNull(this.id);
	}

	public boolean isEdicao() {
		return Objects.nonNull(this.id);
	}

}
