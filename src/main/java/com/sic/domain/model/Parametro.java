package com.sic.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import lombok.Data;

@Data
@Entity
@Table(name = "parametro")
@DynamicInsert
@DynamicUpdate
public class Parametro {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "tipoParam")
	@Enumerated(EnumType.STRING)
	private TipoParametroEnum tipoParam;

	@NotEmpty(message = "e-mail setor de compras")
	private String emailCompras;

	@Enumerated(EnumType.STRING)
	private PrioridadeEnum prioridadeUm;
	
	@Enumerated(EnumType.STRING)
	private PrioridadeEnum prioridadeDois;
	
	@Enumerated(EnumType.STRING)
	private PrioridadeEnum prioridadeTres;
}
