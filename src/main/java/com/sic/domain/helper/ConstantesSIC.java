package com.sic.domain.helper;

public class ConstantesSIC {
	
	public static final String MS_TOKEN = "authenticate";
	public static final String ENVIO_EMAIL_SIMPLES = "email/envio";
	public static final String ENVIO_EMAIL_LISTA = "email/lista";
	public static final String ENVIO_EMAIL_COTACAO = "email/cotacao";
	public static final String PRODUTO = "produto";
	public static final String CADASTRO_FORNECEDOR = "fornecedor";
	public static final String GRAVAR_COTACAO = "cotacao/gravar";
	public static final String LOAD_COTACAO = "cotacao/load";
	public static final String PROPOSTA_LOAD = "proposta/load";
	public static final String PROPOSTA_GRAVA = "proposta/grava";
	public static final String COTACAO = "cotacao";
}
