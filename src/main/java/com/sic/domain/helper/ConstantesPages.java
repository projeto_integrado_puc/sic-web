package com.sic.domain.helper;

public class ConstantesPages {

	public static final String LOGIN = "/login";
	public static final String ERRO_403 = "/403";
	public static final String DASHBOARD = "/";
	public static final String REDEFINIR = "/usuario/redefinir";
	public static final String SENHA = "/senha";
	public static final String LOGOUT = "/logout";
	public static final String CADASTROUSUARIO = "/cadastroUsuario";
	public static final String PESQUISAUSUARIO = "/pesquisaUsuario";
	public static final String SUCESSO = "/sucesso";
	public static final String CADASTROCOTACAO = "/cadastroCotacao";	
	public static final String SUCESSOCOTACAO = "/sucessocotacao";
	public static final String SUCESSOPROPOSTA = "/sucessoproposta";
	public static final String SUCESSOPARAMETRO= "/sucessoparametro";
	public static final String COTACAOFINALIZADA= "/cotacaofinalizada";
	
	public static final String API = "/api/**";
}