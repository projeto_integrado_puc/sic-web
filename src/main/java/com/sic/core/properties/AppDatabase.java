package com.sic.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties("app.database")
public class AppDatabase {

	private String driver;
	private String timezone;

}
