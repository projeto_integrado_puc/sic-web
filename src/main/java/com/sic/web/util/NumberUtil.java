package com.sic.web.util;

import java.math.BigDecimal;
import java.util.Objects;

import org.springframework.util.StringUtils;

public class NumberUtil {
	public static BigDecimal StringParaBigDecimal(String number) {
		BigDecimal retorno = BigDecimal.ZERO;
		if(!StringUtils.isEmpty(number)) {
			String valor = number.replace(".", "");
			retorno = new BigDecimal(valor.replace(',', '.'));
		}
		return retorno;
	}
	
	public static String BigDecimalParaString(BigDecimal number) {
		String retorno = "0,00";
		if(Objects.nonNull(number)) {
			retorno = number.toString().replace('.',',');
		}
		return retorno;
	}
}
