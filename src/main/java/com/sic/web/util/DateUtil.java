package com.sic.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import org.springframework.util.StringUtils;

public class DateUtil {
	
	private final static String pattern = "yyyy-MM-dd";
	private final static String patternFormatada = "dd-MM-yyyy";

	public static Date StringParseDate(String data) throws ParseException {
		return (StringUtils.isEmpty(data)) ? null : new SimpleDateFormat(pattern).parse(data);
	}

	public static String dateParseString(Date data) throws ParseException {
		return (Objects.nonNull(data)) ? new SimpleDateFormat(pattern).format(data) : null;
	}
	
	public static String dataFormatada(Date data) throws ParseException {
		return (Objects.nonNull(data)) ? new SimpleDateFormat(patternFormatada).format(data) : null;
	}
}
