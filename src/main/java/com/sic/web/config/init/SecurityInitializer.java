package com.sic.web.config.init;

import java.util.EnumSet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.SessionTrackingMode;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.filter.CharacterEncodingFilter;

import com.sic.web.security.config.SecurityConfig;

@Configuration
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

	public SecurityInitializer() {
		super(SecurityConfig.class);
	}

	@Override
	protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
		// servletContext.getSessionCookieConfig().setMaxAge(20); //Tempo da sessão
		// ativa
		servletContext.setSessionTrackingModes(EnumSet.of(SessionTrackingMode.COOKIE));

		FilterRegistration.Dynamic characterEncodingFilter = servletContext.addFilter("encodingFilter", new CharacterEncodingFilter());
		characterEncodingFilter.setInitParameter("encoding", "UTF-8");
		characterEncodingFilter.setInitParameter("forceEncoding", "true");
		characterEncodingFilter.addMappingForUrlPatterns(null, false, "/*");
	}

}
