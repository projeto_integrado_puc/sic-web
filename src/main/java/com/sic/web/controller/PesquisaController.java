package com.sic.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sic.domain.service.UsuarioService;

@Controller
@RequestMapping("/pesquisaUsuario")
public class PesquisaController {

	@Autowired
	private UsuarioService service;
	
	@GetMapping(path = "/findAll")
	public ModelAndView listaUsuario() {
		ModelAndView mv = new ModelAndView("pesquisaUsuario");
		mv.addObject("usuarios", service.findAll());
		return mv;
	}
}
