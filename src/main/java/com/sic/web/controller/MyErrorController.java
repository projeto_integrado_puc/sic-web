package com.sic.web.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MyErrorController implements ErrorController {

	@GetMapping("/error")
	public String handleError(HttpServletRequest request) {
		Integer statusCode = (Integer) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

		if (statusCode == HttpStatus.NOT_FOUND.value()) {
			return "404";
		} else if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
			return "500";
		}

		return "404";
	}

	public String getErrorPath() {
		return "/404";
	}

}
