package com.sic.web.controller;

import java.util.Arrays;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sic.domain.dto.RedefinirSenhaDTO;
import com.sic.domain.exception.EmailJaCadastradoException;
import com.sic.domain.exception.SenhaObrigatoriaUsuarioException;
import com.sic.domain.model.OcupacaoEnum;
import com.sic.domain.model.Usuario;
import com.sic.domain.service.UsuarioService;
import com.sic.domain.service.UsuarioSistemaService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	@Autowired
	private UsuarioSistemaService usuarioSistema;

	@GetMapping(path="cadastrar")
	public ModelAndView novo(@ModelAttribute("usuario") Usuario usuario) {
		ModelAndView mv = new ModelAndView("cadastroUsuario");
		mv.addObject("ocupacoes", Arrays.asList(OcupacaoEnum.ADMINISTRADOR, OcupacaoEnum.ADMINISTRADOR));
		Usuario usuarioNew = new Usuario();
		usuarioNew.setUsername("login.nome@email.com");
		mv.addObject("usuario", usuarioNew);
		mv.addObject("titulo", "Criar usuário");
		mv.addObject("fielDisable", false);
		return mv;
	}
	
	@GetMapping(path="/editar")
	public ModelAndView novo(@RequestParam(value = "id", required = false) Long id) {
		Optional<Usuario> opUsuario = this.service.findById(id);
		ModelAndView mv = new ModelAndView("cadastroUsuario");
		mv.addObject("ocupacoes", Arrays.asList(OcupacaoEnum.ADMINISTRADOR, OcupacaoEnum.ADMINISTRADOR));
		mv.addObject("usuario", opUsuario.get());
		mv.addObject("titulo", "Editar usuário");
		mv.addObject("fielDisable", false);
		return mv;
	}
	
	 @GetMapping("/paginada")
	  public ModelAndView getEmployees(@PageableDefault(size = 10) Pageable pageable,
	                             Model model) {
		 ModelAndView mv = new ModelAndView("consultaUsuario");
		 Page<Usuario> page = null;
		 if(usuarioSistema.isUsuarioAdministrador()) {
			 page = service.findAll(pageable);			 
		 }else {
			 page = service.findAllByUserName(usuarioSistema.getUserName(), pageable);
		 }
	      mv.addObject("page", page);
	      return mv;
	  }

	@PostMapping
	public String cadastrar(@Valid @ModelAttribute("usuario") Usuario usuario, BindingResult result) {

		if (result.hasErrors()) {
			return "cadastroUsuario";
		}

		try {
			service.salvar(usuario);
		} catch (EmailJaCadastradoException e) {
			result.rejectValue("username", e.getMessage(), e.getMessage());
			return "cadastroUsuario";
		} catch (SenhaObrigatoriaUsuarioException e) {
			result.rejectValue("senha", e.getMessage(), e.getMessage());
			return "cadastroUsuario";
		}

		return "redirect:/usuario/paginada";
	}
	
	@GetMapping("/redefinir")
	public ModelAndView redefinirSenha(@ModelAttribute("redefinir") RedefinirSenhaDTO redefinir) {
		return new ModelAndView("esqueciSenha");
	}
}
