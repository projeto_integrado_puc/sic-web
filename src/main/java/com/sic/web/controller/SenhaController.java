package com.sic.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sic.domain.dto.RedefinirSenhaDTO;
import com.sic.domain.exception.UsuarioNaoCadastradoException;
import com.sic.domain.service.UsuarioService;

@Controller
@RequestMapping("/senha")
public class SenhaController {

	@Autowired
	private UsuarioService service;
	
	@PostMapping
	public ModelAndView cadastrar(@Valid @ModelAttribute("redefinir") RedefinirSenhaDTO redefinir, BindingResult result,
			RedirectAttributes attributes) {

		if (result.hasErrors()) {
			return new ModelAndView("esqueciSenha");
		}

		try {
			service.redefinirSenha(redefinir);
		} catch (UsuarioNaoCadastradoException e) {
			result.rejectValue("username", e.getMessage(), e.getMessage());
			return new ModelAndView("esqueciSenha");
		}

		attributes.addFlashAttribute("mensagem", "Senha alterada com sucesso!");
		return new ModelAndView("redirect:/redefinir");
	}
}
