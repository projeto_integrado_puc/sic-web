package com.sic.web.controller;

import java.util.Arrays;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sic.domain.dto.ParametroGeralDTO;
import com.sic.domain.helper.ConstantesPages;
import com.sic.domain.model.Parametro;
import com.sic.domain.model.PrioridadeEnum;
import com.sic.domain.service.ParametroService;

@Controller
@RequestMapping("/parametro")
public class ParametroController {
	
	@Autowired
	private ParametroService service;

	@GetMapping(path="load")
	public ModelAndView novo(Model model) {
		ParametroGeralDTO param = new ParametroGeralDTO();
		param.setParametro(service.loadParametroGeral());
		ModelAndView mv = new ModelAndView("cadastroParametro");
		mv.addObject("param", param);
		mv.addObject("listPrioridade", Arrays.asList(PrioridadeEnum.values()));
		return mv;
	}

	@PostMapping
	public ModelAndView cadastrar(@Valid @ModelAttribute("param") ParametroGeralDTO param, BindingResult result) {

		Parametro parametro = service.validaParametro(param, result);
		
		if (result.hasErrors()) {
			ModelAndView mv = new ModelAndView("cadastroParametro");
			mv.addObject("param", param);
			mv.addObject("listPrioridade", Arrays.asList(PrioridadeEnum.values()));
			return mv;
		}

		try {
			service.salvar(parametro);
		} catch (Exception e) {
			ModelAndView mv = new ModelAndView("cadastroParametro");
			mv.addObject("param", param);
			mv.addObject("listPrioridade", Arrays.asList(PrioridadeEnum.values()));
			return mv;
		} 

		ModelAndView mv = new ModelAndView(ConstantesPages.SUCESSOPARAMETRO);
		return mv;
	}
}
