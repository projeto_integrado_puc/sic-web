package com.sic.web.controller;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sic.domain.dto.FornecedorPropostaDTO;
import com.sic.domain.dto.RespostaPropostaDTO;
import com.sic.domain.helper.ConstantesPages;
import com.sic.domain.service.CotacaoService;
import com.sic.domain.service.PropostaService;
import com.sic.domain.service.UsuarioSistemaService;
@Controller
@RequestMapping("/proposta")
public class PropostaController {

	@Autowired
	private PropostaService propostaService;
	
	@Autowired
	private CotacaoService cotacaoService;
	
	@Autowired
	private UsuarioSistemaService usuarioSistemaService;
	
	@GetMapping("/load")
	public ModelAndView getPropostas(@RequestParam(value = "id", required = false) Long id) throws URISyntaxException, ParseException {
		List<FornecedorPropostaDTO> itens = propostaService.loadProposta(usuarioSistemaService.getCNPJ(), id, usuarioSistemaService.isUsuarioAdministrador());
				
		String titulo = "Cotação " + id;
		RespostaPropostaDTO cotacao = new RespostaPropostaDTO(titulo, itens);
	
		ModelAndView mv = new ModelAndView("propostaCotacao");
		mv.addObject("cotacao", cotacao);
		return mv;
	}

	@PostMapping
	public String cadastrar(@Valid @ModelAttribute("cotacao") RespostaPropostaDTO cotacao, BindingResult result) throws URISyntaxException, ParseException {
		
		this.propostaService.validaProposta(cotacao.getPropostas(), result);
		
		if(result.hasErrors()){
			return "propostaCotacao";
		}
		
		this.propostaService.gravar(cotacao.getFornecedorProposta());
		
		return ConstantesPages.SUCESSOPROPOSTA;
	}
	
	@GetMapping("/consulta")
	public ModelAndView getCotacao(@RequestParam(value = "id", required = false) Long id) throws URISyntaxException, ParseException {
		List<FornecedorPropostaDTO> itens = propostaService.loadProposta(usuarioSistemaService.getCNPJ(), id, usuarioSistemaService.isUsuarioAdministrador());
		
		String titulo = "Cotação " + id;
		RespostaPropostaDTO cotacao = new RespostaPropostaDTO(titulo, itens);
	
		ModelAndView mv = new ModelAndView("consultaProposta");
		mv.addObject("cotacao", cotacao);
		return mv;
	}
	
	@GetMapping("/finaliza")
	public String finalizaCotacao(@RequestParam(value = "id", required = false) Long id) throws URISyntaxException, ParseException {
		cotacaoService.finalizaCotacao(id);	
		return ConstantesPages.COTACAOFINALIZADA;
	}
}
