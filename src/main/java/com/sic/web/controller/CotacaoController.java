package com.sic.web.controller;

import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sic.domain.dto.CotacaoDTO;
import com.sic.domain.helper.ConstantesPages;
import com.sic.domain.service.CotacaoService;
import com.sic.domain.service.LegadoService;
@Controller
@RequestMapping("/cotacao")
public class CotacaoController {

	@Autowired
	private LegadoService legadoService;
	
	@Autowired
	private CotacaoService cotacaoService;
	
	@GetMapping("/cadastrar")
	public String getProdutos(Model model) throws URISyntaxException {
		CotacaoDTO cotacao = new CotacaoDTO();
		cotacao.setProdutos(legadoService.getProdutosSemEstoque());
		model.addAttribute("cotacao", cotacao);
		return "cadastroCotacao"; 
	}

	@PostMapping
	public String cadastrar(@Valid @ModelAttribute("cotacao") CotacaoDTO cotacao, BindingResult result) throws URISyntaxException {
		
		this.cotacaoService.validaCotacao(cotacao, result);
		
		if(result.hasErrors()){
			return "cadastroCotacao";
		}
		
		this.cotacaoService.gravaCotacao(cotacao);
		
		return ConstantesPages.SUCESSOCOTACAO;
	}
}
