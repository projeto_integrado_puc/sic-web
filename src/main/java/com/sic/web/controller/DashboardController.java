package com.sic.web.controller;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sic.domain.dto.CotacaoDTO;
import com.sic.domain.service.CotacaoService;
import com.sic.domain.service.UsuarioSistemaService;

@Controller
public class DashboardController {

	private static final String DASHBOARD = "Dashboard";
	
	@Autowired
	private CotacaoService cotacaoService;
	
	@Autowired
	private UsuarioSistemaService usuarioSistemaService;

	@GetMapping("/")
	public ModelAndView dashboard(@PageableDefault(size = 10) Pageable pageable,
            Model model) {
		ModelAndView mv = new ModelAndView(DASHBOARD);
		 Page<CotacaoDTO> page = null;
		 try {
			page = cotacaoService.findCotacaoPage(pageable);
			if(Objects.nonNull(page) && !page.isEmpty()) {
				boolean userAdmin = usuarioSistemaService.isUsuarioAdministrador();
				if(!userAdmin) {
					Date now = new Date();
					page.get().forEach(c -> c.setPermiteConsulta(c.getFimDtCotacao().before(now)));
				}else {
					page.get().forEach(c -> c.setPermiteConsulta(userAdmin));
				}
				page.get().forEach(c -> c.setUserAdmin(userAdmin));
			}
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}			 
		 
	      mv.addObject("page", page);
	      return mv;
	}
}
