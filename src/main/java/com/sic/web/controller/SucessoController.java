package com.sic.web.controller;

import java.net.URISyntaxException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sic.domain.helper.ConstantesPages;

@Controller
@RequestMapping("/sucesso")
public class SucessoController {
		
	@GetMapping("/cotacao")
	public String getMensagemCotacaoSucesso(Model model, @RequestHeader String codCotacao) throws URISyntaxException {
		return ConstantesPages.SUCESSOCOTACAO; 
	}
	
	@GetMapping("/proposta")
	public String getMensagemPropostaSucesso(Model model, @RequestHeader String codCotacao) throws URISyntaxException {
		return ConstantesPages.SUCESSOPROPOSTA; 
	}
	
	@GetMapping("/parametro")
	public String getMensagemParametroSucesso(Model model, @RequestHeader String codCotacao) throws URISyntaxException {
		return ConstantesPages.SUCESSOPARAMETRO; 
	}
}
