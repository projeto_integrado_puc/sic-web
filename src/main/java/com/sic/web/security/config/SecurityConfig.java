package com.sic.web.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.sic.domain.helper.ConstantesPages;
import com.sic.domain.service.UsuarioService;
import com.sic.web.security.AppUserDetailsService;

@EnableWebSecurity
@ComponentScan(basePackageClasses = { AppUserDetailsService.class, UsuarioService.class })
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailsService userDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}
	

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
		   .antMatchers("/layout/**")
		   .antMatchers("/javascripts/**")
		   .antMatchers("/stylesheets/**")
		   .antMatchers("/images/**")
		   .antMatchers(HttpMethod.GET, ConstantesPages.API)
		   .antMatchers(HttpMethod.POST, ConstantesPages.API)
		   .antMatchers(HttpMethod.DELETE, ConstantesPages.API)
		   .mvcMatchers("/swagger-ui.html/**", "/configuration/**", "/swagger-resources/**", "/v2/api-docs", "/webjars/**");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
	      	.authorizeRequests()	  
	      		.antMatchers(ConstantesPages.REDEFINIR, ConstantesPages.SENHA).permitAll()
			.anyRequest().authenticated()				
				.and()
			.formLogin()
				.loginPage(ConstantesPages.LOGIN).permitAll()
				.defaultSuccessUrl(ConstantesPages.DASHBOARD, true)
				.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher(ConstantesPages.LOGOUT))
				.and()				
			.exceptionHandling()
				.accessDeniedPage(ConstantesPages.ERRO_403)
				.and()
			.sessionManagement()
				.invalidSessionUrl(ConstantesPages.LOGIN);
																
	}
	
}
