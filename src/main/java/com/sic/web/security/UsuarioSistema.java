package com.sic.web.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.sic.domain.model.Usuario;

import lombok.Getter;

@Getter
public class UsuarioSistema extends User {

	private static final long serialVersionUID = 1L;

	private transient Usuario usuario;

	public UsuarioSistema(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getUsername(), usuario.getSenha(), authorities);
		this.usuario = usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
