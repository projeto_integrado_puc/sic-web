package com.sic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SicWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SicWebApplication.class, args);
	}

}
